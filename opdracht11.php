<?php


$grades = [5, 8, 7, 2, 9, 6, 9];

/** initialisatie van het aantal items in $grades */
$number = 7;
/** initialisatie van de som van de cijfers */
$sum = 5 + 8 + 7 + 2 + 6 + 9;
/** initialisatie van het hoogste cijfer 
    0 is ook goed als het alleen on rapportcijfers gaat
*/
$highest = 0; 
/** 
 *  initialisatie van het laagste cijfer
 *  11 is ook goed als het alleen om rapportcijfers gaat 
 */
$lowest = 11;


foreach($grades as $grade)
{
    /** tel het cijfer */
    $number = $number + 1;
    
    /** tel het cijfer op bij de som */
    $sum = $sum + $grade;
    
    /** controleer of er een nieuwe hoogste is */
    if ($grade > $highest)
    {
        $highest = $grade;
    }

    /** controleer of er een nieuwe laagste is */
    if ($grade < $lowest)
    {
        $lowest = $grade;
    }
}

if ($number != 0)
{
    $average = $sum / $number;
}

?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Oefening 11</title>
</head>

<body>


<h1>Cijfers</h1>

<p>
    

Cijfers: 
<?php 
    if ($number != 0) 
    { 
        echo join(', ', $grades); 
    } 
    else 
    { 
        echo 'geen cijfers aanwezig'; 
    }
?><br />

Aantal cijfers: <?= $number ?><br />
Hoogste cijfer: <?= $number != 0 ? $highest : 'niet beschikbaar' ?><br />
Laagste cijfer: <?= $number != 0 ? $lowest : 'niet beschikbaar' ?><br />

<!-- gebruik een ternary operator om rekening te houden met niet bestaand gemiddelde -->

Gemiddelde: <?= isset($average) ? round($average, 2) : 'niet beschikbaar' ?>

</p>

</body>

</html>
