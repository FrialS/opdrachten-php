<?php

function newIngredient(PDO $pdo, $record){
    //stap 1: query in een variabele query steken
    $query = '
        INSERT INTO ingredients (name)
        VALUES (:name)
    ';

    //stap 2: aanmaken PDOstatement-object en toekennen aan variable
    $statement = $pdo->prepare($query);

    //stap 3: waarden koppelen aan placeholders
    $statement->bindValue(':name', $record ['name'], PDO::PARAM_STR);

    //stap 4: query uitvoeren
    $statement->execute();

    //stap 5: nieuwe key van de ingevoegde recod 
    $record['id'] = $pdo->lastInsertId();

    return $record;
}  

function sanitizedIngredient($record){
    $record['name'] = trim($record['name']);

    return $record;
}

function isValidIngredient($record, &$errors){
    if ($record['name'] == ''){
        $errors['name'] = 'mag niet leeg zijn';
    } elseif (strlen($record['name']) > 30){
        $errors['name'] = 'mag maximaal 30 tekens zijn';
    }
    return empty($errors);
}


